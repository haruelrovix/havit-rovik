package model

import "time"

// TryOut represents tryOut domain model
type TryOut struct {
	Base
	TestID int   `json:"-"`
	UserID int   `json:"user_id"`
	Test   *Test `json:"test,omitempty"`

	TableName struct{} `sql:"tryouts" json:"-" pg:",discard_unknown_columns"`
}

// TryOutSubmission represents tryOut submission domain model
type TryOutSubmission struct {
	ID            int
	CorrectAnswer int
	NotAnswered   int
	WrongAnswer   int
	Score         int
	SubmittedAt   time.Time

	TableName struct{} `sql:"tryouts" json:"-" pg:",discard_unknown_columns"`
}

// TryOutResult represents Try Out submission result domain model
type TryOutResult struct {
	TryOut
	CorrectAnswer     int       `json:"correct_answer"`
	CorrectPercentage float64   `json:"correct_answer_percentage" sql:"-"`
	Duration          int64     `json:"duration" sql:"-"`
	NotAnswered       int       `json:"not_answered"`
	WrongAnswer       int       `json:"wrong_answer"`
	WrongPercentage   float64   `json:"wrong_answer_percentage" sql:"-"`
	Score             int       `json:"score"`
	SubmittedAt       time.Time `json:"submitted_at"`
	Ranking           int       `json:"ranking" sql:"-"`
}

// TryOutDB represents tryOut database interface (repository)
type TryOutDB interface {
	Create(TryOut) (*TryOut, error)
	View(int, int, bool) (*TryOut, error)
	Update(*TryOutSubmission) (*TryOutResult, error)
	GetRowPosition(int) (*TryOutResult, error)
	GetParticipants(int, string) ([]TryOutResult, error)
}
