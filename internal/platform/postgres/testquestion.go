package pgsql

import (
	"github.com/go-pg/pg"
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
)

// NewTestQuestionDB returns a new TestDB instance
func NewTestQuestionDB(c *pg.DB, l echo.Logger) *TestQuestionDB {
	return &TestQuestionDB{c, l}
}

// TestQuestionDB represents the client for test_question table
type TestQuestionDB struct {
	cl  *pg.DB
	log echo.Logger
}

// Create creates a new test-question on database
func (u *TestQuestionDB) Create(tq *[]model.TestQuestion) ([]model.TestQuestion, error) {
	testQuestion := (*tq)[:]

	if err := u.cl.Insert(&testQuestion); err != nil {
		u.log.Error("TestQuestionDB Error: %v", err)
		return nil, err
	}

	return nil, nil
}
