package pgsql

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
)

// NewQuestionDB returns a new QuestionDB instance
func NewQuestionDB(c *pg.DB, l echo.Logger) *QuestionDB {
	return &QuestionDB{c, l}
}

// QuestionDB represents the client for question table
type QuestionDB struct {
	cl  *pg.DB
	log echo.Logger
}

// Create creates a new question on database
func (u *QuestionDB) Create(question model.Question) (*model.Question, error) {
	if err := u.cl.Insert(&question); err != nil {
		u.log.Error("QuestionDB Error: %v", err)
		return nil, err
	}
	return &question, nil
}

// List returns list of all questions
func (u *QuestionDB) List(qp *model.ListQuery, p *model.Pagination) ([]model.Question, error) {
	var questions []model.Question

	// Prepare the Query
	q := u.cl.Model(&questions).
		Column("question.*").
		Relation("Answers", func(o *orm.Query) (*orm.Query, error) {
			return o, nil
		})

	if p != nil {
		q.Limit(p.Limit).Offset(p.Offset)
	}

	if qp != nil {
		q.Where(qp.Query, qp.ID)
	}

	q.Where(notDeleted)

	// Execute the query
	if err := q.Select(); err != nil {
		u.log.Warnf("QuestionDB Error: %v", err)
		return nil, err
	}

	return questions, nil
}
