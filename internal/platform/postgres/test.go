package pgsql

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
)

// NewTestDB returns a new TestDB instance
func NewTestDB(c *pg.DB, l echo.Logger) *TestDB {
	return &TestDB{c, l}
}

// TestDB represents the client for tests table
type TestDB struct {
	cl  *pg.DB
	log echo.Logger
}

// Create creates a new test on database
func (u *TestDB) Create(test model.Test) (*model.Test, error) {
	if err := u.cl.Insert(&test); err != nil {
		u.log.Error("TestDB Error: %v", err)
		return nil, err
	}
	return &test, nil
}

// List returns list of all test
func (u *TestDB) List(qp *model.ListQuery, p *model.Pagination, isStudent bool) ([]model.Test, error) {
	var tests []model.Test

	// Prepare the Query, order by tests_questions Index
	q := u.cl.Model(&tests)

	// Student should not allowed to see Questions for the Test
	if !isStudent {
		q.Column("test.*", "Questions", "Questions.Answers").
			Relation("Questions", func(o *orm.Query) (*orm.Query, error) {
				return o.Order("index ASC"), nil
			}).
			Relation("Questions.Answers", func(o *orm.Query) (*orm.Query, error) {
				return o, nil
			})
	}

	if p != nil {
		q.Limit(p.Limit).Offset(p.Offset)
	}

	if qp != nil {
		q.Where(qp.Query, qp.ID)
	}

	q.Where(notDeleted)

	// Execute the query
	if err := q.Select(); err != nil {
		u.log.Warnf("TestDB Error: %v", err)
		return nil, err
	}

	return tests, nil
}

// View returns single test by ID
func (u *TestDB) View(id int, isStudent bool) (*model.Test, error) {
	// Get test
	test, err := u.List(&model.ListQuery{Query: "id = ?", ID: id}, nil, isStudent)
	if err != nil {
		u.log.Warnf("TestDB Error: %v", err)
		return nil, err
	}

	return &test[0], err
}
