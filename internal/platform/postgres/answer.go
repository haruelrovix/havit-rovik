package pgsql

import (
	"github.com/go-pg/pg"
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
)

// NewAnswerDB returns a new answer instance
func NewAnswerDB(c *pg.DB, l echo.Logger) *AnswerDB {
	return &AnswerDB{c, l}
}

// AnswerDB represents the client for answer table
type AnswerDB struct {
	cl  *pg.DB
	log echo.Logger
}

// Create creates a new answer on database
func (u *AnswerDB) Create(tq *[]model.Answer) ([]model.Answer, error) {
	answers := (*tq)[:]

	if err := u.cl.Insert(&answers); err != nil {
		u.log.Error("AnswerDB Error: %v", err)
		return nil, err
	}

	return answers, nil
}
