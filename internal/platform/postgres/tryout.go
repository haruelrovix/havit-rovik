package pgsql

import (
	"fmt"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
)

// NewTryOutDB returns a new TryOutDB instance
func NewTryOutDB(c *pg.DB, l echo.Logger) *TryOutDB {
	return &TryOutDB{c, l}
}

// TryOutDB represents the client for tryout table
type TryOutDB struct {
	cl  *pg.DB
	log echo.Logger
}

// Create creates a new tryout on database
func (to *TryOutDB) Create(tryOut model.TryOut) (*model.TryOut, error) {
	// Has User been participated on the Try Out already?
	_, err := to.cl.Model(&tryOut).Where("test_id = ?test_id AND user_id = ?user_id").SelectOrInsert()
	if err != nil {
		to.log.Error("TryOutDB Error: %v", err)
		return nil, err
	}

	// Returns the Try Out WITH the Test (list of Questions, of course)
	tryOutByID, err := to.View(tryOut.ID, tryOut.UserID, true)
	if err != nil {
		return nil, err
	}

	return tryOutByID, nil
}

// View returns single tryout by ID
func (to *TryOutDB) View(id int, userID int, isPartial bool) (*model.TryOut, error) {
	var tryOut = new(model.TryOut)

	q := to.cl.Model(tryOut).
		Column("try_out.*", "Test", "Test.Questions")

	if isPartial {
		// Select all of Answers columns except for correct_answer
		q.Column("Test.Questions.Answers.id", "Test.Questions.Answers.answer", "Test.Questions.Answers.question_id").
			Column("Test.Questions.Answers.created_at", "Test.Questions.Answers.updated_at")
	} else {
		q.Column("Test.Questions.Answers")
	}

	q.Relation("Test", func(o *orm.Query) (*orm.Query, error) {
		return o, nil
	}).
		Relation("Test.Questions", func(o *orm.Query) (*orm.Query, error) {
			return o.Order("index ASC"), nil
		}).
		Relation("Test.Questions.Answers", func(o *orm.Query) (*orm.Query, error) {
			return o, nil
		}).
		Where("try_out.id = ? AND try_out.user_id = ?", id, userID)

	// Execute the query
	if err := q.Select(); err != nil {
		to.log.Warnf("TryOutDB Error: %v", err)
		return nil, fmt.Errorf("Try Out with ID:%d doesn't belong to current user", id)
	}

	return tryOut, nil
}

// Update updates user's Try Out info
func (to *TryOutDB) Update(tryOut *model.TryOutSubmission) (*model.TryOutResult, error) {
	_, err := to.cl.Model(tryOut).WherePK().Update()
	if err != nil {
		to.log.Warnf("TryOutDB Error: %v", err)
	}

	// Ranking, if another participants have the same right
	result, err := to.GetRowPosition(tryOut.ID)
	if err != nil {
		to.log.Warnf("TryOutDB Error: %v", err)
	}

	return result, err
}

// GetRowPosition within rows result given tryout id
func (to *TryOutDB) GetRowPosition(id int) (*model.TryOutResult, error) {
	participants, err := to.GetParticipants(id, "id")
	if err != nil {
		to.log.Warnf("TryOutDB Error: %v", err)
	}

	var tryOut = new(model.TryOutResult)

	if len(participants) > 0 {
		tryOut = &participants[0]
	}

	return tryOut, nil
}

// GetParticipants get rows for given test_id
func (to *TryOutDB) GetParticipants(id int, attr string) ([]model.TryOutResult, error) {
	var tryOut []model.TryOutResult

	template := `SELECT *
		FROM (
			SELECT *, ROW_NUMBER() OVER (
				ORDER BY score DESC NULLS LAST, correct_answer DESC NULLS LAST, (submitted_at - created_at) ASC
			) AS ranking
			FROM tryouts
		) t
		WHERE %s = %d`

	sql := fmt.Sprintf(template, attr, id)

	_, err := to.cl.Query(&tryOut, sql)
	if err != nil {
		fmt.Println(err)
		to.log.Warnf("TryOutDB Error: %v", err)
	}
	return tryOut, err
}
