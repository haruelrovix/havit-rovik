// Package test contains test application services
package test

import (
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
	"github.com/haruelrovix/tryout/internal/platform/query"
)

// New creates new test application service
func New(qdb model.TestDB, rbac model.RBACService, auth model.AuthService) *Service {
	return &Service{qdb: qdb, rbac: rbac, auth: auth}
}

// Service represents test application service
type Service struct {
	qdb  model.TestDB
	rbac model.RBACService
	auth model.AuthService
}

// Create creates a new test
func (s *Service) Create(c echo.Context, req model.Test) (*model.Test, error) {
	u := s.auth.User(c)
	_, err := query.List(u)
	if err != nil {
		return nil, err
	}

	return s.qdb.Create(req)
}

// List returns list of test
func (s *Service) List(c echo.Context, p *model.Pagination) ([]model.Test, error) {
	u := s.auth.User(c)
	if err := s.rbac.EnforceRole(c, model.UserRole); err != nil {
		return nil, err
	}

	return s.qdb.List(nil, p, u.Role == model.UserRole)
}

// View returns single test
func (s *Service) View(c echo.Context, id int) (*model.Test, error) {
	if err := s.rbac.EnforceUser(c, id); err != nil {
		return nil, err
	}

	return s.qdb.View(id, false)
}
