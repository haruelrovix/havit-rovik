// Package tryout contains tryOut application services
package tryout

import (
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
)

// New creates new tryOut application service
func New(todb model.TryOutDB, rbac model.RBACService, auth model.AuthService) *Service {
	return &Service{todb: todb, rbac: rbac, auth: auth}
}

// Service represents tryOut application service
type Service struct {
	todb model.TryOutDB
	rbac model.RBACService
	auth model.AuthService
}

// Create creates a new tryOut
func (s *Service) Create(c echo.Context, req model.TryOut) (*model.TryOut, error) {
	u := s.auth.User(c)
	req.UserID = u.ID

	if err := s.rbac.EnforceRole(c, model.UserRole); err != nil {
		return nil, err
	}

	return s.todb.Create(req)
}

// Get get tryOut participants
func (s *Service) Get(c echo.Context, req model.TryOut) ([]model.TryOutResult, error) {
	u := s.auth.User(c)
	if u.Role == model.UserRole {
		return s.todb.GetParticipants(u.ID, "user_id")
	}

	return s.todb.GetParticipants(req.TestID, "test_id")
}

// Submit Questions-Answers for Try Out
func (s *Service) Submit(c echo.Context, submission *model.TryOutSubmission) (*model.TryOutResult, error) {
	if err := s.rbac.EnforceRole(c, model.UserRole); err != nil {
		return nil, err
	}

	return s.todb.Update(submission)
}

// View returns single Try Out
func (s *Service) View(c echo.Context, id int) (*model.TryOut, error) {
	if err := s.rbac.EnforceRole(c, model.UserRole); err != nil {
		return nil, err
	}

	u := s.auth.User(c)

	return s.todb.View(id, u.ID, false)
}
