// Package question contains question application services
package question

import (
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
	"github.com/haruelrovix/tryout/internal/platform/query"
)

// New creates new question application service
func New(qdb model.QuestionDB, rbac model.RBACService, auth model.AuthService) *Service {
	return &Service{qdb: qdb, rbac: rbac, auth: auth}
}

// Service represents question application service
type Service struct {
	qdb  model.QuestionDB
	rbac model.RBACService
	auth model.AuthService
}

// Create creates a new question
func (s *Service) Create(c echo.Context, req model.Question) (*model.Question, error) {
	u := s.auth.User(c)
	_, err := query.List(u)
	if err != nil {
		return nil, err
	}

	return s.qdb.Create(req)
}

// List returns list of question
func (s *Service) List(c echo.Context, p *model.Pagination) ([]model.Question, error) {
	u := s.auth.User(c)
	q, err := query.List(u)
	if err != nil {
		return nil, err
	}

	return s.qdb.List(q, p)
}
