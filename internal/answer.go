package model

// Answer represents answer domain model
type Answer struct {
	Base
	Answer        string `json:"answer"`
	CorrectAnswer bool   `json:"correct_answer"`
	QuestionID    int    `json:"question_id"`
}

// AnswerDB represents answer database interface (repository)
type AnswerDB interface {
	Create(*[]Answer) ([]Answer, error)
	// List(*ListQuery, *Pagination) ([]Test, error)
	// View(int) (*Test, error)
}
