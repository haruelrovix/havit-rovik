package model

// Question represents question domain model
type Question struct {
	Base
	Question string   `json:"question"`
	Answers  []Answer `json:"answers"`

	TableName struct{} `json:"-" pg:",discard_unknown_columns"`
}

// QuestionDB represents question database interface (repository)
type QuestionDB interface {
	Create(Question) (*Question, error)
	List(*ListQuery, *Pagination) ([]Question, error)
}
