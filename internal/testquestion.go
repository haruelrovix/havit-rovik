package model

// TestQuestion represents test_question domain model
type TestQuestion struct {
	QuestionID int `json:"question_id"`
	TestID     int `json:"test_id"`
	Index      int

	TableName struct{} `sql:"tests_questions"`
}

// TestQuestionDB represents test_question database interface (repository)
type TestQuestionDB interface {
	Create(*[]TestQuestion) ([]TestQuestion, error)
}
