// Package testquestion contains test-question application services
package testquestion

import (
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
	"github.com/haruelrovix/tryout/internal/platform/query"
)

// New creates new test-question application service
func New(qdb model.TestQuestionDB, rbac model.RBACService, auth model.AuthService) *Service {
	return &Service{qdb: qdb, rbac: rbac, auth: auth}
}

// Service represents test-question application service
type Service struct {
	qdb  model.TestQuestionDB
	rbac model.RBACService
	auth model.AuthService
}

// Create creates a new test-question
func (s *Service) Create(c echo.Context, req *[]model.TestQuestion) ([]model.TestQuestion, error) {
	u := s.auth.User(c)
	_, err := query.List(u)
	if err != nil {
		return nil, err
	}

	return s.qdb.Create(req)
}
