// Package answer contains answer application services
package answer

import (
	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
	"github.com/haruelrovix/tryout/internal/platform/query"
)

// New creates new test-question application service
func New(adb model.AnswerDB, rbac model.RBACService, auth model.AuthService) *Service {
	return &Service{adb: adb, rbac: rbac, auth: auth}
}

// Service represents answer application service
type Service struct {
	adb  model.AnswerDB
	rbac model.RBACService
	auth model.AuthService
}

// Create creates a new answer
func (s *Service) Create(c echo.Context, req *[]model.Answer) ([]model.Answer, error) {
	u := s.auth.User(c)
	_, err := query.List(u)
	if err != nil {
		return nil, err
	}

	return s.adb.Create(req)
}
