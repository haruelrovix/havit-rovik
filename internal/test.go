package model

// Test represents test domain model
type Test struct {
	Base
	Name              string `json:"name"`
	RightAnswerWeight int    `json:"right_answer_weight"`
	TimeLimit         int64  `json:"time_limit"`
	WrongAnswerWeight int    `json:"wrong_answer_weight"`

	Questions []Question `json:"questions,omitempty" pg:"many2many:tests_questions"`
}

// TestDB represents test database interface (repository)
type TestDB interface {
	Create(Test) (*Test, error)
	List(*ListQuery, *Pagination, bool) ([]Test, error)
	View(int, bool) (*Test, error)
}
