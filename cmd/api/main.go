// Copyright 2017 Emir Ribic. All rights reserved.
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file.

// GORSK - Go(lang) restful starter kit
//
// API Docs for GORSK v1
//
// 	 Terms Of Service:  N/A
//     Schemes: http
//     Version: 1.0.0
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: Emir Ribic <ribice@gmail.com> https://ribice.ba
//     Host: localhost:8080
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - bearer: []
//
//     SecurityDefinitions:
//     bearer:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta
package main

import (
	"github.com/go-pg/pg"

	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/cmd/api/config"
	"github.com/haruelrovix/tryout/cmd/api/mw"
	"github.com/haruelrovix/tryout/cmd/api/server"
	"github.com/haruelrovix/tryout/cmd/api/service"

	"github.com/haruelrovix/tryout/internal/account"
	"github.com/haruelrovix/tryout/internal/answer"
	"github.com/haruelrovix/tryout/internal/auth"
	"github.com/haruelrovix/tryout/internal/platform/postgres"
	"github.com/haruelrovix/tryout/internal/question"
	"github.com/haruelrovix/tryout/internal/rbac"
	"github.com/haruelrovix/tryout/internal/test"
	"github.com/haruelrovix/tryout/internal/testquestion"
	"github.com/haruelrovix/tryout/internal/tryout"
	"github.com/haruelrovix/tryout/internal/user"

	_ "github.com/haruelrovix/tryout/cmd/api/swagger"
)

func main() {

	cfg, err := config.Load("dev")
	checkErr(err)

	e := server.New()

	db, err := pgsql.New(cfg.DB)
	checkErr(err)

	addV1Services(cfg, e, db)

	server.Start(e, cfg.Server)
}

func addV1Services(cfg *config.Configuration, e *echo.Echo, db *pg.DB) {

	// Initialize DB interfaces

	userDB := pgsql.NewUserDB(db, e.Logger)
	accDB := pgsql.NewAccountDB(db, e.Logger)
	answerDB := pgsql.NewAnswerDB(db, e.Logger)
	questionDB := pgsql.NewQuestionDB(db, e.Logger)
	testDB := pgsql.NewTestDB(db, e.Logger)
	testQuestionDB := pgsql.NewTestQuestionDB(db, e.Logger)
	tryOutDB := pgsql.NewTryOutDB(db, e.Logger)

	// Initialize services

	jwt := mw.NewJWT(cfg.JWT)
	authSvc := auth.New(userDB, jwt)
	service.NewAuth(authSvc, e, jwt.MWFunc())

	e.Static("/swaggerui", "cmd/api/swaggerui")

	rbacSvc := rbac.New(userDB)

	v1Router := e.Group("/v1")

	v1Router.Use(jwt.MWFunc())

	// Workaround for Echo's issue with routing.
	// v1Router should be passed to service normally, and then the group name created there
	uR := v1Router.Group("/users")
	service.NewAccount(account.New(accDB, userDB, rbacSvc), uR)
	service.NewUser(user.New(userDB, rbacSvc, authSvc), uR)

	qR := v1Router.Group("/questions")
	service.NewQuestion(
		question.New(questionDB, rbacSvc, authSvc),
		answer.New(answerDB, rbacSvc, authSvc),
		qR,
	)

	tR := v1Router.Group("/tests")
	service.NewTest(
		test.New(testDB, rbacSvc, authSvc),
		testquestion.New(testQuestionDB, rbacSvc, authSvc),
		tR,
	)

	toR := v1Router.Group("/tryout")
	service.NewTryOut(
		tryout.New(tryOutDB, rbacSvc, authSvc),
		test.New(testDB, rbacSvc, authSvc),
		toR,
	)
}

func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}
