package swagger

import (
	"github.com/haruelrovix/tryout/cmd/api/request"
	"github.com/haruelrovix/tryout/internal"
)

// Question create request
// swagger:parameters queCreate
type swaggQueCreateReq struct {
	// in:body
	Body request.Register
}

// Questions model response
// swagger:response questionListResp
type swaggQuestionListResponse struct {
	// in:body
	Body struct {
		Questions []model.Question `json:"questions"`
		Page      int              `json:"page"`
	}
}
