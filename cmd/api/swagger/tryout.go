package swagger

import (
	"github.com/haruelrovix/tryout/cmd/api/request"
)

// TryOut create request
// swagger:parameters tryOutCreate
type swaggTryOutCreateReq struct {
	// in:body
	Body request.Register
}
