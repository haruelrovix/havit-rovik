package swagger

import (
	"github.com/haruelrovix/tryout/cmd/api/request"
	"github.com/haruelrovix/tryout/internal"
)

// Test create request
// swagger:parameters testCreate
type swaggTestCreateReq struct {
	// in:body
	Body request.Register
}

// Tests model response
// swagger:response testListResp
type swaggTestListResponse struct {
	// in:body
	Body struct {
		Tests []model.Test `json:"tests"`
		Page  int          `json:"page"`
	}
}
