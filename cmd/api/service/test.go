package service

import (
	"net/http"

	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
	"github.com/haruelrovix/tryout/internal/test"
	"github.com/haruelrovix/tryout/internal/testquestion"

	"github.com/haruelrovix/tryout/cmd/api/request"
)

// Test represents test http service
type Test struct {
	svc  *test.Service
	qsvc *testquestion.Service
}

// NewTest creates new test http service
func NewTest(svc *test.Service, qsvc *testquestion.Service, tr *echo.Group) {
	t := Test{svc: svc, qsvc: qsvc}
	// swagger:route POST /v1/tests tests testCreate
	// Creates new test.
	// responses:
	//  200: testResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	tr.POST("", t.create)
	// swagger:operation GET /v1/tests tests listTests
	// ---
	// summary: Returns list of test.
	// description: Returns list of test.
	// parameters:
	// - name: limit
	//   in: query
	//   description: number of results
	//   type: int
	//   required: false
	// - name: page
	//   in: query
	//   description: page number
	//   type: int
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/testListResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	tr.GET("", t.list)
}

type testListResponse struct {
	Tests []model.Test `json:"tests"`
	Page  int          `json:"page"`
}

func (t *Test) create(c echo.Context) error {
	r, err := request.TestCreate(c)
	if err != nil {
		return err
	}

	// Insert Test
	test, err := t.svc.Create(c, model.Test{
		Name:              r.Name,
		RightAnswerWeight: r.RightAnswerWeight,
		TimeLimit:         r.TimeLimit,
		WrongAnswerWeight: r.WrongAnswerWeight,
	})
	if err != nil {
		return err
	}

	// Populate Test-Question
	var testQuestion []model.TestQuestion
	for i, questionID := range r.Questions {
		testQuestion = append(testQuestion, model.TestQuestion{
			TestID:     test.ID,
			QuestionID: questionID,
			Index:      i + 1,
		})
	}

	// Insert Test-Question
	_, err = t.qsvc.Create(c, &testQuestion)
	if err != nil {
		return err
	}

	// Get Questions and assign it to the Test
	result, err := t.svc.View(c, test.ID)
	if err != nil {
		return err
	}

	test.Questions = result.Questions

	return c.JSON(http.StatusOK, test)
}

func (t *Test) list(c echo.Context) error {
	p, err := request.Paginate(c)
	if err != nil {
		return err
	}
	result, err := t.svc.List(c, &model.Pagination{
		Limit: p.Limit, Offset: p.Offset,
	})
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, testListResponse{result, p.Page})
}
