package service

import (
	"errors"
	"net/http"
	"time"

	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
	"github.com/haruelrovix/tryout/internal/test"
	"github.com/haruelrovix/tryout/internal/tryout"

	"github.com/haruelrovix/tryout/cmd/api/request"
)

// TryOut represents tryOut http service
type TryOut struct {
	svc  *tryout.Service
	tsvc *test.Service
}

// NewTryOut creates new tryOut http service
func NewTryOut(svc *tryout.Service, tsvc *test.Service, tor *echo.Group) {
	to := TryOut{svc: svc, tsvc: tsvc}
	// swagger:route POST /v1/tryout tryout tryOutCreate
	// Creates new Try Out.
	// responses:
	//  200: TryOut
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	tor.POST("", to.create)
	// swagger:route GET /v1/tryout/{id} tryout getParticipants
	// Gets Try Out participants.
	// responses:
	//  200: TryOutResult
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	tor.GET("/:id", to.getParticipants)
	// swagger:operation PATCH /v1/tryout/{id}/submit tryout tryOutSubmit
	// ---
	// summary: Submit answers for the Try Out.
	// parameters:
	// - name: id
	//   in: path
	//   description: ID of the Try Out
	//   type: int
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	tor.PATCH("/:id/submit", to.submit)
}

func (to *TryOut) create(c echo.Context) error {
	r, err := request.TryOutCreate(c)
	if err != nil {
		return err
	}

	// Insert TryOut
	tryOut, err := to.svc.Create(c, model.TryOut{
		TestID: r.TestID,
	})
	if err != nil {
		return err
	}

	// Time limit to complete the Try Out
	duration := time.Now().Sub(tryOut.CreatedAt).Nanoseconds() / int64(time.Millisecond)
	if duration > tryOut.Test.TimeLimit {
		return errors.New("You have participated on this Try Out")
	}

	return c.JSON(http.StatusOK, tryOut)
}

func (to *TryOut) getParticipants(c echo.Context) error {
	id, err := request.ID(c)
	if err != nil {
		return err
	}

	// Get TryOut
	tryOut, err := to.svc.Get(c, model.TryOut{
		TestID: id,
	})
	if err != nil {
		return err
	}

	for i, val := range tryOut {
		totalQuestions := val.CorrectAnswer + val.WrongAnswer + val.NotAnswered

		// Percentage of correct anwers / wrong answers
		correctPercentage := float64(val.CorrectAnswer) / float64(totalQuestions) * 100
		tryOut[i].CorrectPercentage = correctPercentage
		tryOut[i].WrongPercentage = float64(val.WrongAnswer) / float64(totalQuestions) * 100

		// Duration to complete the test
		tryOut[i].Duration = (val.SubmittedAt.Sub(val.CreatedAt)).Nanoseconds()
	}

	return c.JSON(http.StatusOK, tryOut)
}

// QuestionAnswer represents the map version of Question and Answers relation
type QuestionAnswer struct {
	ID              int
	Question        string
	Answers         map[int]model.Answer
	CorrectAnswerID int
}

func (to *TryOut) submit(c echo.Context) error {
	submittedAt := time.Now()

	r, err := request.TryOutSubmit(c)
	if err != nil {
		return err
	}

	// Get the Test, Questions and its Answers for this Try Out
	tryOut, err := to.svc.View(c, r.ID)
	if err != nil {
		return err
	}

	// Time limit to complete the Try Out
	duration := submittedAt.Sub(tryOut.CreatedAt).Nanoseconds() / int64(time.Millisecond)
	if duration > tryOut.Test.TimeLimit {
		return errors.New("Time limit for Try Out has been exceeded")
	}

	// Create a map to make calculation easier
	questions := make(map[int]QuestionAnswer)
	for _, question := range tryOut.Test.Questions {
		answers := make(map[int]model.Answer)
		var correctAnswer int
		for _, answer := range question.Answers {
			answers[answer.ID] = answer

			if answer.CorrectAnswer {
				correctAnswer = answer.ID
			}
		}

		questions[question.ID] = QuestionAnswer{
			ID:              question.ID,
			Question:        question.Question,
			Answers:         answers,
			CorrectAnswerID: correctAnswer,
		}
	}

	// Calculate how many correct vs wrong answer
	correctAnswer := 0
	wrongAnswer := 0
	for _, submission := range r.Submission {
		if question, ok := questions[submission.QuestionID]; ok {
			if question.CorrectAnswerID == submission.AnswerID {
				correctAnswer++
			} else {
				wrongAnswer++
			}
		}
	}

	// Scoring
	totalQuestions := len(tryOut.Test.Questions)
	notAnswered := totalQuestions - (correctAnswer + wrongAnswer)
	score := (correctAnswer * tryOut.Test.RightAnswerWeight) + (wrongAnswer * tryOut.Test.WrongAnswerWeight)

	// Submit the calculation result to DB
	result, err := to.svc.Submit(c, &model.TryOutSubmission{
		ID:            r.ID,
		CorrectAnswer: correctAnswer,
		NotAnswered:   notAnswered,
		WrongAnswer:   wrongAnswer,
		Score:         score,
		SubmittedAt:   submittedAt,
	})
	if err != nil {
		return err
	}

	// Percentage of correct anwers / wrong answers
	result.CorrectPercentage = float64(correctAnswer) / float64(totalQuestions) * 100
	result.WrongPercentage = float64(wrongAnswer) / float64(totalQuestions) * 100

	// Duration to complete the test
	result.Duration = (submittedAt.Sub(tryOut.CreatedAt)).Nanoseconds()

	return c.JSON(http.StatusOK, result)
}
