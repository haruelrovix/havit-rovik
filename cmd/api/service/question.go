package service

import (
	"net/http"

	"github.com/labstack/echo"

	"github.com/haruelrovix/tryout/internal"
	"github.com/haruelrovix/tryout/internal/answer"
	"github.com/haruelrovix/tryout/internal/question"

	"github.com/haruelrovix/tryout/cmd/api/request"
)

// Question represents user http service
type Question struct {
	svc *question.Service
	avc *answer.Service
}

// NewQuestion creates new question http service
func NewQuestion(svc *question.Service, avc *answer.Service, qr *echo.Group) {
	q := Question{svc: svc, avc: avc}
	// swagger:route POST /v1/questions questions queCreate
	// Creates new question.
	// responses:
	//  200: questionResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	qr.POST("", q.create)
	// swagger:operation GET /v1/questions questions listQuestions
	// ---
	// summary: Returns list of question.
	// description: Returns list of question.
	// parameters:
	// - name: limit
	//   in: query
	//   description: number of results
	//   type: int
	//   required: false
	// - name: page
	//   in: query
	//   description: page number
	//   type: int
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/questionListResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	qr.GET("", q.list)
}

type questionListResponse struct {
	Questions []model.Question `json:"questions"`
	Page      int              `json:"page"`
}

func (q *Question) create(c echo.Context) error {
	r, err := request.QuestionCreate(c)
	if err != nil {
		return err
	}

	// Insert Question
	question, err := q.svc.Create(c, model.Question{
		Question: r.Question,
	})
	if err != nil {
		return err
	}

	// Populate Answers
	var answers []model.Answer
	for _, answer := range r.Answers {
		correctAnswer := false
		if answer == r.CorrectAnswer {
			correctAnswer = true
		}

		answers = append(answers, model.Answer{
			Answer:        answer,
			QuestionID:    question.ID,
			CorrectAnswer: correctAnswer,
		})
	}

	// Insert Answers
	_, err = q.avc.Create(c, &answers)
	if err != nil {
		return err
	}

	// Assign Answers
	question.Answers = answers

	return c.JSON(http.StatusOK, question)
}

func (q *Question) list(c echo.Context) error {
	p, err := request.Paginate(c)
	if err != nil {
		return err
	}
	result, err := q.svc.List(c, &model.Pagination{
		Limit: p.Limit, Offset: p.Offset,
	})
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, questionListResponse{result, p.Page})
}
