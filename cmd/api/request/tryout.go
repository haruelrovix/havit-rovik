package request

import (
	"github.com/labstack/echo"
)

// TryOutRegister contains tryOut creation request
type TryOutRegister struct {
	TestID int `json:"test_id" validate:"required"`
}

// TryOutSubmission contains tryOut submission request
type TryOutSubmission struct {
	ID         int              `json:"-"`
	Submission []QuestionAnswer `json:"submission"`
}

// QuestionAnswer contains keypair of Question ID and Answer ID
type QuestionAnswer struct {
	QuestionID int `json:"question_id"`
	AnswerID   int `json:"answer_id"`
}

// TryOutCreate validates tryOut creation request
func TryOutCreate(c echo.Context) (*TryOutRegister, error) {
	r := new(TryOutRegister)
	if err := c.Bind(r); err != nil {
		return nil, err
	}
	return r, nil
}

// TryOutSubmit validates tryOut submission request
func TryOutSubmit(c echo.Context) (*TryOutSubmission, error) {
	id, err := ID(c)
	if err != nil {
		return nil, err
	}

	r := new(TryOutSubmission)
	if err := c.Bind(r); err != nil {
		return nil, err
	}

	r.ID = id

	return r, nil
}
