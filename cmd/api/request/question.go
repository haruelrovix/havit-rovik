package request

import (
	"errors"

	"github.com/labstack/echo"
)

// QueRegister contains question registration request
type QueRegister struct {
	Question      string   `json:"question" validate:"required"`
	Answers       []string `json:"answers" validate:"required,min=2"`
	CorrectAnswer string   `json:"correct_answer" validate:"required"`
}

// QuestionCreate validates question creation request
func QuestionCreate(c echo.Context) (*QueRegister, error) {
	r := new(QueRegister)
	if err := c.Bind(r); err != nil {
		return nil, err
	}

	// CorrectAnswer should exist within Answers
	found := false
	for i := range r.Answers {
		if r.Answers[i] == r.CorrectAnswer {
			// Found!
			found = true
			break
		}
	}

	if !found {
		return nil, errors.New("A correct answer should exist within answers")
	}

	return r, nil
}
