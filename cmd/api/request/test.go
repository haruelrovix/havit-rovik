package request

import (
	"github.com/labstack/echo"
)

// TestRegister contains test registration request
type TestRegister struct {
	Name              string `json:"name" validate:"required"`
	Questions         []int  `json:"questions" validate:"required,min=5,max=10"`
	RightAnswerWeight int    `json:"right_answer_weight" validate:"required"`
	TimeLimit         int64  `json:"time_limit" validate:"required"`
	WrongAnswerWeight int    `json:"wrong_answer_weight" validate:"required"`
}

// TestCreate validates account creation request
func TestCreate(c echo.Context) (*TestRegister, error) {
	r := new(TestRegister)
	if err := c.Bind(r); err != nil {
		return nil, err
	}
	return r, nil
}
