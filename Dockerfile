# Dockerfile extending the generic Go image with application files for a
# single application.
FROM gcr.io/google-appengine/golang

ENV GOPATH /go

# The files which are copied are specified in the .dockerignore file
COPY . /go/src/github.com/haruelrovix/tryout/

WORKDIR /go/src/github.com/haruelrovix/tryout/

RUN go build -o dist/bin/tryout ./cmd/api

# All configuration parameters are passed through environment variables and specified in app.yaml
CMD ["/go/src/github.com/haruelrovix/tryout/dist/bin/tryout"]